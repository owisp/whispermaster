package registry

import (
	"log"
	"os/exec"
	"strings"

	"github.com/golang/protobuf/proto"
)

type Replier struct {
	owisps *OWisps
}

type OWisps struct {
	store *RegistryStore
}

func NewOWispsRegistry() (*OWisps, error) {
	store, err := NewStore("")
	if err != nil {
		return nil, err
	}
	return &OWisps{
		store: store,
	}, nil
}

func (registry *OWisps) Replier() *Replier {
	return &Replier{
		owisps: registry,
	}
}

func (registry *OWisps) newOwisp() string {
	out, err := exec.Command("uuidgen").Output()
	if err != nil {
		log.Printf("Failed to generate id: %v", err)
		return "" // TODO error handling
	}
	//TODO should check id in store
	id := strings.Trim(string(out), "\n")
	registry.store.storeOWisp(&StoredOWisp{
		Id: id,
	})
	return id
}

func (rep *Replier) Reply(req []byte) []byte {
	regReq := RegisterOWisp{}
	if err := proto.Unmarshal(req, &regReq); err != nil {
		log.Printf("Failed to unmarshall register request: %v", err)
		return []byte("ERROR") // TODO error handling
	}
	log.Printf("Received register request: %v", regReq)

	if regReq.Id == "" {
		return rep.newOwisp()
	}

	//TODO should update db registry and return proper result
	return []byte("OK")
}

func (rep *Replier) newOwisp() []byte {
	id := rep.owisps.newOwisp()
	regRes := RegisterOWisp{
		Id: id,
	}
	resp, err := proto.Marshal(&regRes)
	if err != nil {
		log.Printf("Failed to marshall registry response: %v", err)
		return []byte("ERROR") // TODO error handling
	}
	log.Printf("Responding to new owisp with id %s", regRes.Id)
	return resp
}
