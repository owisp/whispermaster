package registry

import (
	"log"
	"os/user"

	"github.com/dgraph-io/badger"
	"github.com/golang/protobuf/proto"
)

type RegistryStore struct {
	db *badger.DB
}

func (store *RegistryStore) Close() {
	store.db.Close()
}

func userDir() (string, error) {
	usr, err := user.Current()
	if err != nil {
		return "", err
	}
	return usr.HomeDir, nil
}

func NewStore(dir string) (*RegistryStore, error) {

	if dir == "" {
		home, err := userDir()
		if err != nil {
			return nil, err
		}
		dir = home + "/.owisp"
	}

	// if stat, err := os.Stat(dir); os.IsNotExist(err) {
	// 	os.Mkdir(dir, 0600)
	// } else {
	// 	switch mode := stat.Mode(); {
	// 	case mode.IsRegular():
	// 		return nil, fmt.Errorf("File %s is regular, but expected directory to store data", dir)
	// 	}
	// }

	opts := badger.DefaultOptions
	opts.Dir = dir
	opts.ValueDir = dir

	db, err := badger.Open(opts)
	if err != nil {
		log.Printf("Failed to open registry store: %v", err)
		return nil, err
	}

	// defer db.Close()
	//TODO should close db somewhere

	return &RegistryStore{
		db: db,
	}, nil
}

func (store *RegistryStore) storeOWisp(owisp *StoredOWisp) error {
	marshalled, err := proto.Marshal(owisp)
	if err != nil {
		log.Printf("Error while marhshalling owisp: %v", err)
		return err
	}
	err = store.db.Update(func(txn *badger.Txn) error {
		err := txn.Set([]byte(owisp.Id), marshalled)
		return err
	})
	if err != nil {
		log.Printf("Error while storing owisp: %v", err)
		return err
	}
	log.Printf("Stored new owisp: %v", owisp)
	return nil
}
