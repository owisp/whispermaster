timestamp	=	$(shell date '+%Y-%m-%d_%H.%M.%S')
version		= 	$(shell git describe --exact-match --tags $$(git log -n1 --pretty='%h'))
hash		=	$(shell git rev-list -1 HEAD)

dev: owisp-vue-dist go

.PHONY: owisp-vue-dist
owisp-vue-dist:
	cd owisp; npm run build

.PHONY: go
go:
	go install

.PHONY: release
release: owisp-vue-dist go-release 

.PHONY: go-release
go-release: go-release-install

.PHONY: go-release-install
go-release-install:
	go install -ldflags "-X main.timestamp=$(timestamp) -X main.version=$(version) -X main.hash=$(hash)"

.PHONY: go-release-build
go-release-build:
	go build -ldflags "-X main.timestamp=$(timestamp) -X main.version=$(version) -X main.hash=$(hash)"

protobuf: protobuf-server protobuf-client

.PHONY: protobuf-server
protobuf-server:
	cd registry && protoc --go_out=. registry.proto

.PHONY: protobuf-client
protobuf-client:
	cd whisperagent/registry && protoc --go_out=. -I ../../registry/ registry.proto
