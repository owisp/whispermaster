package req

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"nanomsg.org/go/mangos/v2"
	"nanomsg.org/go/mangos/v2/protocol/rep"
	"nanomsg.org/go/mangos/v2/transport/ws"
)

func die(format string, v ...interface{}) {
	log.Printf(format, v...)
	fmt.Fprintln(os.Stderr, fmt.Sprintf(format, v...))
	os.Exit(1)
}

type Replier interface {
	Reply(req []byte) []byte
}

func reqHandler(sock mangos.Socket, replier Replier) {
	for {
		req, e := sock.Recv()
		log.Printf("receiving...")
		if e != nil {
			die("Cannot get request: %v", e)
		}
		reply := replier.Reply(req)
		if e := sock.Send(reply); e != nil {
			die("Cannot send reply: %v", e)
		}
	}
}

func AddHandler(r *mux.Router, port int, path string, replier Replier) {
	log.Printf("adding handler")
	sock, _ := rep.NewSocket()

	// sock.AddTransport(ws.NewTransport())

	url := fmt.Sprintf("ws://:%d/%s", port, path)
	if l, e := sock.NewListener(url, nil); e != nil {
		die("bad listener: %v", e)
	} else if h, e := l.GetOption(ws.OptionWebSocketHandler); e != nil {
		die("bad handler: %v", e)
	} else {
		l.SetOption(ws.OptionWebSocketCheckOrigin, false)
		r.Handle("/"+path, h.(http.Handler))
		l.Listen()
	}
	log.Printf("Run mangos rep")
	go reqHandler(sock, replier)
}
