package main

import (
	"fmt"
	"os"
	"syscall/js"
)

func main() {
	fmt.Println("Hello, WebAssembly!")

	var cb js.Func
	cb = js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		// name:=js.Global().Get("vue").Get("$refs").Get("hello_tale_by_golang").Get("name").String()
		name := "Unknown traveler"
		if len(args) > 0 {
			name = args[0].String()
		}

		fmt.Println("Callback execute")
		js.Global().Call("alert", fmt.Sprintf("Hello, %s!", name))
		// cb.Release() // release the function if the button will not be clicked again
		return nil
	})

	fmt.Println("Registering callback")
	if len(os.Args) < 2 {
		fmt.Println("Require at least two arguments")
	}

	key := os.Args[1]

	js.Global().
		Get("window").
		Get(key).
		Set("myAwesomeCallback", cb)

	done := make(chan bool) //TODO add kill
	<-done
	cb.Release()
}
