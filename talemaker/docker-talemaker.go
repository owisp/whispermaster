package talemaker

import (
	"fmt"
	"log"
	"os"
	"os/exec"
)

func checkDocker() error {
	executable := "docker"
	if bts, err := exec.Command(executable, "version").Output(); err != nil {
		log.Printf("Error, cannot find executable %s", executable)
		return fmt.Errorf("Error, cannot find executable %s", executable)
	} else {
		log.Printf("Found docker version: \n%s", string(bts))
	}
	return nil
}

func runMake(talefile TaleFile) error {
	dir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	image := ""
	volumes := []string{}

	for _, opt := range talefile.makeropts {
		switch opt.name {
		case "IMAGE":
			image = opt.value
		case "VOLUME":
			volumes = append(volumes, opt.value)
		}
	}

	if image == "" {
		// maybe there is a Dockerfile to build image ?
		if taleFileStat, err := os.Stat("Dockerfile"); !os.IsNotExist(err) {
			switch mode := taleFileStat.Mode(); {
			case mode.IsRegular():
				log.Printf("Discovered Dockerfile, use it to build talemaker")
				image = "talemaker_" + talefile.Name
				buildTaleMakerImage(image)
			}
		}
	}

	// image := "rust-talemaker"
	// volumes := []string{":/src"}
	args := []string{"run", "--name", "talemaker_for_" + talefile.Name, "--rm", "-it"}

	for _, volume := range volumes {
		args = append(args, "-v", fmt.Sprintf("%s/%s", dir, volume))
	}

	args = append(args, image)

	// log.Printf("docker run --name rust-talemaker --rm -it -v $PWD:/src rust-talemaker")
	log.Printf("Executing docker %v", args)
	cmd := exec.Command("docker", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	err = cmd.Run()
	if err != nil {
		log.Printf("Failed to make the tale")
		return err
	}
	return nil

}

func buildTaleMakerImage(tag string) {
	args := []string{"build", "-t", tag, "."}

	log.Printf("Executing docker %v", args)
	cmd := exec.Command("docker", args...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Failed to make the tale")
	}
}
