package talemaker

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

type TaleMakerOpt struct {
	name  string
	value string
}

// TaleFile describes tale building and running process
type TaleFile struct {
	maker         string
	makeropts     []TaleMakerOpt
	Componentfile string
	Servedir      string
	Name          string
}

func readTaleFile(path string) TaleFile {
	log.Printf("Reading talefile %s", path)
	talefile := TaleFile{}
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		trimmed := strings.Trim(line, " ")
		if trimmed == "" || strings.HasPrefix(trimmed, "#") {
			continue
		}

		log.Printf("Reading line: %s", line)
		splitten := strings.Split(line, " ")
		if len(splitten) < 2 {
			log.Fatalf("line %s contains error: %s", line, "should be more than one words in line")
		} else {
			operand := splitten[0]
			first := splitten[1]
			switch operand {
			case "MAKER":
				talefile.maker = first
			case "NAME":
				talefile.Name = first
			case "MAKEROPT":
				if len(splitten) < 3 {
					log.Fatalf("line %s contains error: %s", line, "option value not specified")
				} else {
					talefile.makeropts = append(talefile.makeropts, TaleMakerOpt{
						name:  first,
						value: splitten[2],
					})
				}
			case "COMPONENTFILE":
				talefile.Componentfile = first
			case "SERVEDIR": //TODO support multiple SERVEDIR
				talefile.Servedir = first
			}
		}

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return talefile
}

func Make() (*TaleFile, error) {
	log.Printf("Making the tale")
	talefile := readTaleFile("Talefile")
	if err := checkDocker(); err != nil {
		log.Printf("Failed to make the tale")
		return &talefile, fmt.Errorf("Failed to make the tale: %v", err)
	}
	return &talefile, runMake(talefile)
}

func (talefile *TaleFile) CheckForInstall() {
	if serveDirStat, err := os.Stat(talefile.Servedir); os.IsNotExist(err) {
		switch mode := serveDirStat.Mode(); {
		case mode.IsRegular():
			log.Fatalf("Expected SERVEDER %s to be directory, but it is a file", talefile.Servedir)
		}
	}

	if compFileStat, err := os.Stat(talefile.Componentfile); os.IsNotExist(err) {
		switch mode := compFileStat.Mode(); {
		case mode.IsDir():
			log.Fatalf("Expected COMPONENTFILE %s to be directory, but it is a file", talefile.Componentfile)
		}
	}
}

func Install() {
	log.Printf("Installing the tale...")
	talefile := readTaleFile("Talefile")

	talefile.CheckForInstall()

	log.Printf("Installing tale %s ...", talefile.Name)

}
