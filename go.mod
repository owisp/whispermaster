module gitlab.com/owisp/whispermaster

go 1.12

require (
	github.com/AndreasBriese/bbloom v0.0.0-20190306092124-e2d15f34fcf9 // indirect
	github.com/dgraph-io/badger v1.5.4
	github.com/dgryski/go-farm v0.0.0-20190416075124-e1214b5e05dc // indirect
	github.com/gobuffalo/packr v1.25.0
	github.com/golang/protobuf v1.3.1
	github.com/gorilla/mux v1.7.1
	github.com/vugu/vugu v0.0.0-20190407194430-15edec0dc2a4 // indirect
	google.golang.org/grpc v1.20.1 // indirect
	nanomsg.org/go/mangos/v2 v2.0.2
)
