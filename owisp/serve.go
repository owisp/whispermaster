package owisp

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"

	"os"
	"regexp"
	"strings"

	"gitlab.com/owisp/whispermaster/talemaker"

	"gitlab.com/owisp/whispermaster/registry"
	"gitlab.com/owisp/whispermaster/req"

	"github.com/gobuffalo/packr"
	"github.com/gorilla/mux"
)

type Tale struct {
	Name              string `json:"name"`
	ComponentFile     string `json:"componentFile"`
	ServeDir          string `json:"serveDir"`
	ComponentMain     string `json:"componentMain"`
	ComponentTemplate string `json:"componentTemplate"`
	Default           bool   `json:"default"`
}

var (
	// defaultTaleName      = flag.String("defaultTaleName", "hello_tale_by_rust", "Name of default tale to load")
	// defaultComponentFile = flag.String("defaultComponentFile", "HelloTaleByRust.vue", "Component file for default tale")
	// defaultServeDir      = flag.String("defaultServeDir", "web", "Folder with js and wasm files for default tale to serve")

	remoteOWisp = flag.String("remoteOWisp", "", "Specify to proxy requests to /owisp to remote server instead of boxed default")
	tales       map[string]*Tale
	jsFile      = regexp.MustCompile("\\.js$")
	cssFile     = regexp.MustCompile("\\.css$")
)

func serveComponentTemplate(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	talename := vars["tale"]
	w.Header().Add("Content-Type", "text/html")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, tales[talename].ComponentTemplate)
}

func serveComponentMain(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	talename := vars["tale"]
	w.Header().Add("Content-Type", "application/javascript")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, tales[talename].ComponentMain)
}

func (tale *Tale) PrepareServe() {

	log.Printf("Processing main for %s", tale.ComponentFile)
	var template bytes.Buffer
	var main bytes.Buffer
	file, err := os.Open(tale.ComponentFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	state := "WAIT_TEMPLATE"
scanning:
	for scanner.Scan() {
		line := strings.Trim(scanner.Text(), " ")
		if line == "" {
			continue
		}
		switch state {
		case "WAIT_TEMPLATE":
			if !strings.HasPrefix(line, "<template>") {
				log.Fatalf("Component file must start with <template>")
			} else {
				template.WriteString(strings.TrimPrefix(line, "<template>"))
				template.WriteByte('\n')
				state = "WAIT_TEMPLATE_END"
			}
		case "WAIT_TEMPLATE_END":
			if !strings.HasSuffix(line, "</template>") {
				template.WriteString(line)
				template.WriteByte('\n')
			} else {
				template.WriteString(strings.TrimSuffix(line, "</template>"))
				template.WriteByte('\n')
				state = "WAIT_SCRIPT_START"
			}
		case "WAIT_SCRIPT_START":
			if !strings.HasPrefix(line, "<script>") {
				log.Fatalf("Expect <script> after template")
			} else {
				main.WriteString(strings.TrimPrefix(line, "<script>"))
				main.WriteByte('\n')
				state = "WAIT_SCRIPT_END"
			}
		case "WAIT_SCRIPT_END":
			if !strings.HasSuffix(line, "</script>") {
				main.WriteString(line)
				main.WriteByte('\n')
			} else {
				main.WriteString(strings.TrimSuffix(line, "</script>"))
				main.WriteByte('\n')
				break scanning
			}
		}
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	tale.ComponentTemplate = template.String()
	tale.ComponentMain = main.String()

}

func serveAllTales(w http.ResponseWriter, r *http.Request) {
	marshalled, err := json.Marshal(tales)
	if err != nil {
		w.Header().Add("Content-Type", "text/plain")
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, fmt.Sprintf("error: %v", err))
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(marshalled)
}

func Serve() error {
	port := 8100
	box := packr.NewBox("./dist")
	owispsRegistry, err := registry.NewOWispsRegistry()
	if err != nil {
		return err
	}

	if taleFileStat, err := os.Stat("Talefile"); !os.IsNotExist(err) {
		switch mode := taleFileStat.Mode(); {
		case mode.IsRegular():
			log.Printf("Discovered Talefile in current directory, install the root tale...")
		}
	}

	if taleFile, err := talemaker.Make(); err != nil {
		log.Printf("Cannot install tale, make failed. Skipping")
		tales = map[string]*Tale{}
	} else {
		log.Printf("Installing root tale")
		tales = map[string]*Tale{
			taleFile.Name: &Tale{
				Name:          taleFile.Name,
				ComponentFile: taleFile.Componentfile,
				ServeDir:      taleFile.Servedir,
				Default:       true, // root tale would be loaded by default
			},
		}
	}

	r := mux.NewRouter()

	req.AddHandler(r, port, "whispermaster/sockets/registry", owispsRegistry.Replier())

	for _, tale := range tales {
		tale.PrepareServe()
		prefix := fmt.Sprintf("/tales/%s/%s/", tale.Name, tale.ServeDir)

		r.PathPrefix(prefix).Handler(
			http.StripPrefix(prefix, http.FileServer(http.Dir(tale.ServeDir))),
		)
		log.Printf("Tale '%s' installed", tale.Name)
	}

	r.HandleFunc("/tales/{tale}/main.js", serveComponentMain)
	r.HandleFunc("/tales/{tale}/template.html", serveComponentTemplate)
	r.HandleFunc("/tales", serveAllTales)

	if *remoteOWisp != "" {
		log.Printf("OWisp would be served in proxy %s", *remoteOWisp)
		serveOWispProxy(*remoteOWisp, r)
	} else {
		log.Printf("OWisp would be served from box")
		r.PathPrefix("/owisp/").Handler(
			http.StripPrefix("/owisp/",
				// wrapMimes(http.FileServer(box))),
				http.FileServer(box)),
		)
	}

	// http.Handle("/", http.FileServer(box))
	http.Handle("/", r)

	url := fmt.Sprintf(":%d", port)
	log.Printf("Serving on %s", url)
	return http.ListenAndServe(url, nil)
}

func wrapMimes(fileserver http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ruri := r.RequestURI
		log.Printf("check MIME for %s", ruri)
		if jsFile.MatchString(ruri) {
			log.Printf("js MIME detected for %s", ruri)
			w.Header().Set("Content-Type", "application/javascript")
		} else if cssFile.MatchString(ruri) {
			log.Printf("css MIME detected for %s", ruri)
			w.Header().Set("Content-Type", "application/css")
		}
		fileserver.ServeHTTP(w, r)
	})
}

func serveOWispProxy(target string, r *mux.Router) {
	remote, err := url.Parse(target)
	if err != nil {
		panic(err)
	}
	proxy := httputil.NewSingleHostReverseProxy(remote)
	r.HandleFunc("/owisp/{rest:.*}", proxyHandler(remote, proxy))
}

func proxyHandler(target *url.URL, p *httputil.ReverseProxy) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		r.URL.Host = target.Host
		r.URL.Scheme = target.Scheme
		r.Header.Set("X-Forwarded-Host", r.Header.Get("Host"))
		r.Host = target.Host

		r.URL.Path = "/owisp/" + mux.Vars(r)["rest"]
		p.ServeHTTP(w, r)
	}
}
