import Vue from 'vue'
import DemoApp from './DemoApp.vue'
import LoadScript from 'vue-plugin-load-script';

Vue.config.productionTip = false


window.demoApp = new Vue({
  render: h => h(DemoApp),
}).$mount('#app').$children[0]

Vue.use(LoadScript);

