package main

import (
	"flag"
	"log"

	"gitlab.com/owisp/whispermaster/owisp"
	"gitlab.com/owisp/whispermaster/talemaker"
)

var (
	timestamp string
	version   string
	hash      string

	argIndex int
)

func main() {
	if version == "" {
		log.Printf("Starting development version of whispermaster")
	} else {
		log.Printf("Starting whispermaster build of version: %s, build timestamp: %s, hash: %s\n", version, timestamp, hash)
	}
	flag.Parse()
	argIndex = 0

	if argIs("serve") {
		log.Fatal(owisp.Serve())
	} else if argIs("tale") {
		if argIs("make") {
			if !argEx() {
				talemaker.Make()
			} else {
				if argIs("install") {

				}
			}
		} else {
			//TODO help
		}
	} else {
		//TODO help
	}

}

func argEx() bool {
	return len(flag.Args()) > argIndex
}

func argIs(arg string) bool {
	if argEx() && flag.Args()[argIndex] == arg {
		argIndex++
		return true
	}
	return false
}
