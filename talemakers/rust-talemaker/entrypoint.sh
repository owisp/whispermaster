#/bin/bash

disable_index_update="-Z no-index-update"
if [[ "${ENABLE_INDEX_UPDATE}" == "true" ]] ; then 
    disable_index_update=""
fi 

echo "Copying /src to $PWD"
cp -r /src/* ./
echo "wasm pack in $PWD"
wasm-pack build --out-dir /src/web --target web -- ${disable_index_update}
