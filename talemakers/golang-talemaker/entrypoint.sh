#/bin/bash

mkdir -p web
cat << EOF > ./web/.gitignore
*
EOF

cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" ./web
GOOS=js GOARCH=wasm go build -o web/main.wasm
