package storage

import "syscall/js"

var (
	Local = NewLocal()
)

type LocalStorage struct {
	storage js.Value
}

func (local *LocalStorage) GetItem(key string) string {
	item := local.storage.Call("getItem", key)
	return item.String()
}

func GetItem(key string) string {
	return Local.GetItem(key)
}

func SetItem(key string, value string) {
	Local.SetItem(key, value)
}

func (local *LocalStorage) SetItem(key string, value string) {
	local.storage.Call("setItem", key, value)
}

func NewLocal() *LocalStorage {
	return &LocalStorage{
		storage: js.Global().Get("localStorage"),
	}
}
