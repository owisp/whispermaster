package owisp

import (
	"fmt"
	"log"
	"strconv"
	"syscall/js"

	proto "github.com/golang/protobuf/proto"
	"gitlab.com/owisp/whispermaster/whisperagent/registry"

	"github.com/nanomsg/mangos/protocol/req"
	wasm "github.com/strowk/mangos-in-browser/client/wasm"
	mangos "nanomsg.org/go/mangos/v2"
)

func NewClientOnPageLocation() *Client {
	host := js.Global().Get("window").Get("location").Get("hostname").String()
	locationPort := js.Global().Get("window").Get("location").Get("port").String()

	log.Printf("Location host %s port %s", host, locationPort)
	port, err := strconv.Atoi(locationPort)
	if err != nil {
		port = 80
	}
	return NewClient(host, port)
}

type Client struct {
	reqSocket mangos.Socket
	host      string
	port      int
}

func NewClient(host string, port int) *Client {
	wasm.Init()
	path := "whispermaster/sockets/registry"
	client := &Client{
		host: host,
		port: port,
	}
	socket, err := client.newReqSocketFor(path)
	if err != nil {
		log.Panicf("Failed to initate socket to %s:%d/%s : %v", host, port, path, err)
	}
	client.reqSocket = socket
	return client
}

func (client *Client) newReqSocketFor(path string) (mangos.Socket, error) {

	sock, e := req.NewSocket()
	if e != nil {
		return nil, fmt.Errorf("cannot make req socket: %v", e)
	}

	// sock.AddTransport(wasm.NewTransport())

	url := fmt.Sprintf("ws://%s:%d/%s", client.host, client.port, path)

	log.Printf("Start dial to %s", url)
	if e = sock.Dial(url); e != nil {
		return nil, fmt.Errorf("cannot dial req url: %v", e)
	}
	log.Printf("Dialed")
	return sock, nil
}

func (client *Client) Register(owisp *OWisp) {
	if owisp.ID != "" {
		client.reconnect(owisp)
	} else {
		owisp.SetID(client.registerAndGetID(owisp))
	}
}

func (client *Client) reconnect(owisp *OWisp) {
	log.Printf("Reconnecting with id from storage: %s", owisp.ID)
	//TODO reconnect
}

func (client *Client) registerAndGetID(owisp *OWisp) string {
	log.Printf("Going to register and receive new ID")
	req := registry.RegisterOWisp{
		Id:     "",
		Spec:   owisp.Spec,
		Status: owisp.Status,
	}
	if data, err := proto.Marshal(&req); err != nil {
		log.Printf("Failed to marshall register request: %v", err)
		return "" //TODO proper error handling
	} else {
		client.reqSocket.Send(data)
		respBytes, err := client.reqSocket.Recv()
		if err != nil {
			log.Printf("Failed to recieve register response: %v", err)
			return "" //TODO proper error handling
		}
		resp := registry.RegisterOWisp{}
		err = proto.Unmarshal(respBytes, &resp)
		if err != nil {
			log.Printf("Failed to unmarshall register response: %v", err)
			return "" //TODO proper error handling
		}
		return resp.Id
	}
}

// func call(sock mangos.Socket,
// 	send []byte,
// 	onsuccess Onsuccess,
// 	onerror Onerror) {
// 	if e := sock.Send(send); e != nil {
// 		log.Printf("Cannot send req: %v", e)
// 	}
// 	log.Printf("Start waiting for reply")
// 	if m, e := sock.Recv(); e != nil {
// 		onerror(e)

// 	} else {
// 		log.Printf("Received reply")
// 		onsuccess(m)

// 	}
// }
