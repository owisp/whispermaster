package owisp

import (
	"fmt"
	"syscall/js"

	"gitlab.com/owisp/whispermaster/whisperagent/registry"
	"gitlab.com/owisp/whispermaster/whisperagent/storage"
)

var (
	Instance *OWisp
)

type OWisp struct {
	ID     string
	Spec   *registry.OWispSpec
	Status *registry.OWispStatus
	cl     *Client
}

func (owisp *OWisp) SetID(id string) {
	storage.SetItem("owispID", id)
	owisp.ID = id
}

func Instantiate(comp *js.Value) *OWisp {
	specTales := comp.
		Get("specTales")

	names := keys(&specTales)

	specWhispers := []*registry.WhisperSpec{}
	statusWhispers := []*registry.WhisperStatus{}

	for _, name := range names {
		isDefault := specTales.Get(name).Get("default").Bool()
		if isDefault { // is included in spec
			specWhispers = append(specWhispers, &registry.WhisperSpec{
				TaleName: name,
			})
			statusWhispers = append(statusWhispers, &registry.WhisperStatus{
				TaleName: name,
				Key:      "", //TODO should pass key from parent component somehow
			})
		}
	}

	Instance = &OWisp{
		ID: storage.GetItem("owispID"),
		Spec: &registry.OWispSpec{
			Whispers: specWhispers,
		},
		Status: &registry.OWispStatus{
			Whispers: statusWhispers,
		},
		cl: NewClientOnPageLocation(),
	}

	if Instance.ID == "null" { // strange string from js
		Instance.ID = ""
	}

	Instance.cl.Register(Instance)

	return Instance
}

func keys(obj *js.Value) []string {
	keysJs := js.Global().Get("Object").Call("keys", obj)
	length := keysJs.Get("length").Int()
	keys := []string{}
	for i := 0; i < length; i++ {
		key := keysJs.Get(fmt.Sprintf("%d", i)).String()
		keys = append(keys, key)
	}
	return keys
}
