package main

import (
	"fmt"
	"log"
	"os"
	"syscall/js"

	"gitlab.com/owisp/whispermaster/whisperagent/owisp"
)

func main() {
	log.Println("Start of whisper agent web assembly")

	if len(os.Args) < 2 {
		fmt.Println("Require at least two arguments")
	}

	key := os.Args[1]

	comp := js.Global().
		Get("window").
		Get(key)

	owisp.Instantiate(&comp)

	done := make(chan bool) //TODO add kill
	<-done

}
