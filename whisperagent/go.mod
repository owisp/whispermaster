module gitlab.com/owisp/whispermaster/whisperagent

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/gopherjs/gopherwasm v1.1.0 // indirect
	github.com/nanomsg/mangos v2.0.0+incompatible
	github.com/strowk/mangos-in-browser v0.0.0-20190422113001-ffee6d727789
	github.com/strowk/websocket v0.0.0-20190421212033-8945673a35fb // indirect
	nanomsg.org/go/mangos/v2 v2.0.2
)
